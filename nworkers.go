package nworkers

type idValue[V any] struct {
	id    int
	value V
}

type Response[V any] struct {
	Id    int
	Value V
	Done  func()
}

type NWorkers[V any] chan idValue[V]

func InitWithValueFunction[V any](size int, value func(id int) V) NWorkers[V] {
	ch := make(chan idValue[V], size)
	for i := 0; i < size; i++ {
		ch <- idValue[V]{
			id:    i,
			value: value(i),
		}
	}
	return ch
}

func (w NWorkers[V]) Get() *Response[V] {
	v := <-w
	return &Response[V]{
		Id:    v.id,
		Value: v.value,
		Done:  func() { w <- v },
	}
}
